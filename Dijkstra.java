import java.util.PriorityQueue;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Dijkstra
{
    //private static List<Vertex> verteces;
    
    public Dijkstra(/*ArrayList<Vertex> verteces*/)
    {
        //this.verteces = verteces;
    }
    
    public static void computePaths(Vertex source)
    {
        source.minDistance = 0.;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
        vertexQueue.add(source);

        while (!vertexQueue.isEmpty()) {
            Vertex u = vertexQueue.poll();

            // Visit each edge exiting u
            for (Edge e : u.adjacencies)
            {
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughU = u.minDistance + weight;
                if (distanceThroughU < v.minDistance) {
                    vertexQueue.remove(v);

                    v.minDistance = distanceThroughU ;
                    v.previous = u;
                    vertexQueue.add(v);
                }
            }
        }
    }

    public static List<Vertex> getShortestPathTo(Vertex target)
    {
        List<Vertex> path = new ArrayList<Vertex>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
            path.add(vertex);

        Collections.reverse(path);
        return path;
    }

    //public static void main(String[] args)
    //{
    /*
    public initialize() {
        // mark all the vertices 
        Vertex E1 = new Vertex("E1", new Location(740, 260));
        Vertex E2 = new Vertex("E2", new Location(670, 260));
        Vertex E3 = new Vertex("E3", new Location(10, 260));
        Vertex E4 = new Vertex("E4", new Location(670, 300));
        Vertex E5 = new Vertex("E5", new Location(716, 300));
        Vertex E6 = new Vertex("E6", new Location(716, 445));
        Vertex E7 = new Vertex("E7", new Location(670, 390));
        Vertex E8 = new Vertex("E8", new Location(10, 390));
        Vertex E9 = new Vertex("E9", new Location(670, 490));
        Vertex E10 = new Vertex("E10", new Location(10, 490));
        Vertex E11 = new Vertex("E11", new Location(670, 675));
        Vertex E12 = new Vertex("E12", new Location(605, 675));
        Vertex E13 = new Vertex("E13", new Location(605, 575));
        Vertex E14 = new Vertex("E14", new Location(145, 675));
        Vertex E15 = new Vertex("E15", new Location(145, 575));
        Vertex E16 = new Vertex("E16", new Location(10, 675));
        Vertex E17 = new Vertex("E17", new Location(120, 675));
        Vertex E18 = new Vertex("E18", new Location(120, 720));
        Vertex E19 = new Vertex("E19", new Location(250, 720));
        Vertex E20 = new Vertex("E20", new Location(340, 390));
        Vertex E21 = new Vertex("E21", new Location(340, 305));

        // set the edges and weight
        E1.adjacencies = new Edge[]{ new Edge(E2, 70) };
        E2.adjacencies = new Edge[]{ new Edge(E3, 660), new Edge(E4, 40) };
        E3.adjacencies = new Edge[]{ new Edge(E2, 660), new Edge(E8, 130) };
        E4.adjacencies = new Edge[]{ new Edge(E2, 40), new Edge(E5, 46), new Edge(E7, 90) };
        E5.adjacencies = new Edge[]{ new Edge(E4, 40), new Edge(E6, 145) };
        E6.adjacencies = new Edge[]{ new Edge(E5, 145) };
        E7.adjacencies = new Edge[]{ new Edge(E4, 90), new Edge(E9, 100), new Edge(E20, 330) };
        E8.adjacencies = new Edge[]{ new Edge(E3, 130), new Edge(E10, 100), new Edge(E20, 330) };
        E9.adjacencies = new Edge[]{ new Edge(E7, 100), new Edge(E10, 660), new Edge(E11, 185) };
        E10.adjacencies = new Edge[]{ new Edge(E8, 100), new Edge(E9, 660), new Edge(E16, 185) };
        E11.adjacencies = new Edge[]{ new Edge(E9, 185), new Edge(E12, 65) };
        E12.adjacencies = new Edge[]{ new Edge(E11, 65), new Edge(E13, 100), new Edge(E14, 460) };
        E13.adjacencies = new Edge[]{ new Edge(E12, 100) };
        E14.adjacencies = new Edge[]{ new Edge(E12, 460), new Edge(E15, 100), new Edge(E17, 25) };
        E15.adjacencies = new Edge[]{ new Edge(E14, 100) };
        E16.adjacencies = new Edge[]{ new Edge(E10, 185), new Edge(E17, 110) };
        E17.adjacencies = new Edge[]{ new Edge(E14, 25), new Edge(E16, 110), new Edge(E18, 45) };
        E18.adjacencies = new Edge[]{ new Edge(E17, 45), new Edge(E19, 130) };
        E19.adjacencies = new Edge[]{ new Edge(E18, 130) };
        E20.adjacencies = new Edge[]{ new Edge(E7, 330), new Edge(E8, 330), new Edge(E21, 85) };
        E21.adjacencies = new Edge[]{ new Edge(E20, 100) };
    }
    */
    
    public static List<Vertex> getShortestPath(Vertex source, Vertex destination)
    {
        computePaths(source); // run Dijkstra
        System.out.println("Distance to " + destination + ": " + destination.minDistance);
        // List<Vertex> path = getShortestPathTo(destination);
        // System.out.println("Path: " + path);
        return getShortestPathTo(destination);
    }
}