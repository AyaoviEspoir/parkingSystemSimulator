/**
 * started on the 05/01/2015.
 */
 
//package test;
 
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import javax.swing.*;
import java.awt.*;
//import java.awt.Toolkit;
//import java.awt.Dimension;

class CheckOnExit implements WindowListener {
   public void windowOpened(WindowEvent e){}
   public void windowClosing(WindowEvent e){
      ConfirmWindow checkers = new ConfirmWindow();
      checkers.setVisible(true);
   }
   public void windowClosed(WindowEvent e){}
   public void windowIconified(WindowEvent e){}
   public void windowDeiconified(WindowEvent e){}
   public void windowActivated(WindowEvent e){}
   public void windowDeactivated(WindowEvent e){}
   
   private class ConfirmWindow extends JFrame implements ActionListener {
      public ConfirmWindow() {
         super("Confirmation Window");
         JFrame.setDefaultLookAndFeelDecorated(true);
         Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
         setBounds((int) screenSize.getWidth()-200,0,200,100);
         setLocationRelativeTo(null);
         //setSize(200,100);
         getContentPane().setBackground(Color.YELLOW);
         setLayout(new BorderLayout());
         setResizable(false);
         
         JLabel confirmLabel = new JLabel("Are you sure you want to exit?");
         add(confirmLabel,BorderLayout.CENTER);
         
         JPanel buttonPanel = new JPanel();
         buttonPanel.setBackground(Color.ORANGE);
         buttonPanel.setLayout(new FlowLayout());
         
         JButton exitButton = new JButton("Yes");
         exitButton.addActionListener(this);
         buttonPanel.add(exitButton);
         
         JButton cancelButton = new JButton("No");
         cancelButton.addActionListener(this);
         buttonPanel.add(cancelButton);
         
         add(buttonPanel, BorderLayout.SOUTH);
      }
      
      public void actionPerformed(ActionEvent e) {
         String actionCommand = e.getActionCommand();
         
         if (actionCommand.equals("Yes"))
            System.exit(0);
         else if (actionCommand.equals("No"))
            dispose();
         else
            System.out.println("Unexpected error in Confirm Window.");
      }  
   }   
}
