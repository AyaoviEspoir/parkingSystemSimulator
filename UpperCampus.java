/**
 * UpperCampus class - Represents top view of UCT upper campus in real time
 * as the simulation is running.
 * 
 * @author Ayaovi Espoir Djissenou
 * @version 02/04/2016
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//
import java.util.concurrent.CountDownLatch;
//
import javax.swing.JButton;
import javax.swing.JPanel;

public class UpperCampus extends JPanel implements Runnable
{
	private static volatile int refreshPeriod;
	// private final int parkingAreaCoords[];	/* coordinates are in the form of x1, y1, x2, y2 */

	public UpperCampus(int ms)
	{
		refreshPeriod = ms;
		// this.parkingAreaCoords = parkingAreaCoords;
	}

	public static void setRefreshPeriod(int newRefreshPeriod) 
	{
		refreshPeriod = newRefreshPeriod;
	}

	public void paintComponent(Graphics g) 
	{
		int width = getWidth();
		int height = getHeight();
		g.clearRect(0, 0, width, height);
		
		g.setColor(Color.gray);
		drawRoads(g);

		// Draw parking areas
		g.setColor(Color.orange);
		for (int i = 0; i < Config.numberOfParkingAreas; i++)
		{
			drawParkingArea(g, i);
		}

		g.setColor(Color.magenta);
		darwBuildings(g);

		g.setColor(Color.black);
		// g.setFont(new Font("Helvetica", Font.PLAIN, 26));
		//draw the words
		//animation must be added
		for (int i = 0; i < Config.movingCars.size(); i++)
		{	    	
			drawCar(g, Config.movingCars.get(i));  //y-offset for skeleton so that you can see the words	
		}
	}

	private void drawCar(Graphics g, Car car)
	{
		int x = (int) car.getLocation().x;
		int y = (int) car.getLocation().y;
		g.fillRect(x - 5, y - 5, 10, 10);
	}

	private void drawParkingArea(Graphics g, int index)
	{
		g.fillRect( Config.parkingAreasCoords[index * 4], Config.parkingAreasCoords[(index * 4) + 1],
					Config.parkingAreasCoords[(index * 4) + 2] - Config.parkingAreasCoords[index * 4], 
					Config.parkingAreasCoords[(index * 4) + 3] - Config.parkingAreasCoords[(index * 4) + 1]);
	}

	private void drawRoads(Graphics g)
	{
		g.fillRect( 0, 480, getWidth(), 20 );
		g.fillRect( 75, 350, getWidth() - 143, 20 );
		g.fillRect( 75, 250, getWidth() - 143, 20);
		g.fillRect( 75, 70, getWidth() - 143, 20);
		g.fillRect( 75, 70, 20, 610 );
		g.fillRect( 735, 70, 20, 430 );
		g.fillRect( 405, 360, 20, 85 );
		g.fillRect( 290, 75, 20, 100 );
		g.fillRect( 590, 75, 20, 100 );
		g.fillRect( 185, 20, 100, 20 );
		g.fillRect( 185, 30, 20, 45 );
		g.fillRect( 745, 440, 56, 20 );
		g.fillRect( 781, 340, 20, 110 );
	}

	private void darwBuildings(Graphics g)
	{
		g.fillRect( 105, 380, 120, 90 );
		g.fillRect( 235, 380, 125, 90 );
		g.fillRect( 470, 380, 125, 90 );
		g.fillRect( 605, 380, 120, 90 );
		g.fillRect( 105, 280, 35, 60 );
		g.fillRect( 150, 280, 125, 60 );
		g.fillRect( 285, 280, 70, 60 );
		g.fillRect( 365, 280, 100, 60 );
		g.fillRect( 475, 280, 70, 60 );
		g.fillRect( 555, 280, 125, 60 );
		g.fillRect( 690, 280, 35, 60 );
	}

	public void run()
	{
		while(!Config.stopSimulation)
		{
			try
			{
				repaint();
            	Thread.sleep(refreshPeriod);   //keep repainting the window every 1 ms
			}
			catch (InterruptedException e)
            {
                e.printStackTrace();
            }
		}
	}
}