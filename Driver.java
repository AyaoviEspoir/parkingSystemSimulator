/**
 * Parking system simulator driver
 * @author Ayaovi Espoir Djissenou
 * @version 26/03/2015
 */

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.event.*;
import java.awt.Toolkit;
import java.awt.Dimension;

import java.util.concurrent.atomic.*;
import java.util.concurrent.*;


class Driver extends JFrame
{
    static final int WINDOW_WIDTH = 825;
    static final int WINDOW_HEIGHT = 750;

    static UpperCampus upper;
    
    public Driver()
    {
        super();
        JFrame.setDefaultLookAndFeelDecorated(true);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((int) screenSize.getWidth() - WINDOW_WIDTH, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        setLocationRelativeTo(null);
        //setSize(WIDTH,HEIGHT);
        setTitle("Parking system simulator");
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new CheckOnExit());

        Config.init();
        upper = new UpperCampus(5);

        add(upper);
    }
    
    public static void main(String args[])
    {
        Driver window = new Driver();

        window.setVisible(true);
        window.setResizable(false);
        Thread campusThread = new Thread(upper);
        Config.run();
        campusThread.start();
        try
        {
            Thread.sleep(10000);
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();               
        }
        Config.stopSimulation = true;

        //Thread timeButtonThread = new Thread(t);
        //timeButtonThread.start();
    }
}