/**
 * Started on the 16/01/2015.
 * By: Ayaovi Espoir Djissenou.
 */
 
//package test;

import java.util.ArrayList;

class Utilities {

   /**
    * List of methods:
    * ===================================================================
    * (public static) String[] addSign(String[] seq);
    * (public static) ArrayList<String> copyToArrayList(String[] seq);
    * (public static) boolean isdigit(String s);
    * (public static) boolean isMathOperator(String s);
    * (public static) void output(ArrayList<String> arrayList);
    * (public static) void output(String[] seq);
    * (public static) void output(int[] seq);
    * (public static) void output(double[] seq);
    * (public static) void output(int[][] arr);
    * (public static) String padWithPlus(String s);
    * (public static) String[] padWithPlus(String[] seq);
    * (public static) String padWithFrontero(String str, int length);
    * (public static) String[] toStringArray(char[] seq);
    * (public static) String[] toStringArray(ArrayList<String> seq);
    * =====================================================================
    **/

   Utilities() {}
   
   /*
    * method
    */
   public static String[] addSign(String[] seq){
   	
      ArrayList<String> arr = new ArrayList<String>();
      int count = 0;
      for(int i = 0;i<seq.length;i++){
         count++;
         arr.add(seq[i]);
         
         if(isdigit(seq[i])){
            if(i==0){
               arr.add(0,"+");
               count++;
            }
            else if(isMathOperator(seq[i]))
               continue;
            else{
               arr.add((count-1),"+");
               count++;  
            }
         }
      }
      return toStringArray(arr);
   }
   
   /*
    * method
    */
   public static ArrayList<String> copyToArrayList(String[] seq){
      
      ArrayList<String> arr = new ArrayList<String>();
      
      for(int i=0;i<seq.length;i++){
         arr.add(seq[i]);
      }
      return arr;
   }

   /*
    * method
    */
   public static boolean isdigit(String s){
   	
      char[] arr = s.toCharArray();
      boolean type = false;
      char[] digits = {'0','1','2','3','4','5','6','7','8','9'};
      
      for(int i=0;i<arr.length;i++){
         for (int j=0;j<digits.length;j++){
            if (digits[j]==arr[i]){
               type = true;
               break;
            }
            else
               type = false;
         }
      }
      return type;   	
   }
   
   /*
    * method
    */
   public static boolean isMathOperator(String s){
   
      String[] operators = {"+","-"};
      
      for (int i=0;i<2;i++){
         if(s.equals(operators[i]))
            return true;
      }
      return false;
   }
   
   /**
    * method.
    */
   public static String output(ArrayList<String> arrayList){
      String s = "[";
      for (int i=0;i<arrayList.size();i++){
         if (i==arrayList.size()-1)
            s += arrayList.get(i);
         else if (i==0)
            s += arrayList.get(i);
         else
            s += (","+arrayList.get(i));
      }
      s += "]";
      
      return s;
   }
   
   /*
    * method
    */
   public static void output(String[] seq){
   
      int len = seq.length;
      System.out.print("[");
      
      for (int i=0;i<len;i++){
         if(i!=(len-1))
            System.out.print("\""+seq[i]+"\", ");
         else
            System.out.print("\""+seq[i]+"\"");
      }
      System.out.println("]");
   }
   
   /*
    * method
    */
   public static void output(double[] seq){
   
      int len = seq.length;
      System.out.print("[");
      
      for (int i=0;i<len;i++){
         if(i!=(len-1))
            System.out.print(seq[i]+",");
         else
            System.out.print(seq[i]);
      }
      System.out.println("]");
   }
   
   /*
    * method
    */
   public static void output(int seq[]){
   
      int len = seq.length;
      System.out.print("[");
      
      for (int i=0;i<len;i++){
         if(i!=(len-1))
            System.out.print(seq[i]+", ");
         else
            System.out.print(seq[i]);
      }
      System.out.println("]");
   }


   /*
    * method
    */
   public static void output(int[][] arr){
   
      int length = arr.length;
      System.out.print("[");
      for (int i=0;i<length;i++){
         if (i!=(length-1)){
            int length2 = arr[i].length;
            for (int j=0;j<length2;j++){
               if (j!=(length2-1))
                  System.out.print("["+arr[i][j]+",");
               else
                  System.out.print(arr[i][j]+"],");
            }
         }
         else{
            int length2 = arr[i].length;
            for (int j=0;j<length2;j++){
               if (j!=(length2-1))
                  System.out.print("["+arr[i][j]+",");
               else
                  System.out.print(arr[i][j]+"]");
            }
         }
      }
      System.out.println("]");
   }
   
   /*
    * method
    */
   public static String padWithFrontZero(String str, int length) {
		/* Assuming the user does not provide a str longer than the lenght */
		int l = str.length();
		if (str.length()<=length) {
			for (int i=0;i<(length-l);i++) {
				str = "0"+str;
				}
			}
		return str;
		}
   
   /*
    * method
    */
   public static String[] padWithPlus(String[] seq){
   
      ArrayList<String> arr = copyToArrayList(seq);
      //if(isdigit(seq[0]))
      if(!isMathOperator(arr.get(0)))
         arr.add(0,"+");
         
      int equal_pos = arr.indexOf("=");
      
      if(!isMathOperator(arr.get(equal_pos+1)))
         arr.add(equal_pos+1,"+");
         
      //return (String[])arr.toArray();
      return toStringArray(arr);
   }
   
   /**
    * method.
    */
   public static String padWithPlus(String s) {
      if (!isMathOperator(s.charAt(0)+""))
         s = "+"+s;
      return s;
   }
   
   /*
    * method
    */
   public static String[] toStringArray(char[] seq){
   
      String[] arr = new String[seq.length];
      for (int i=0;i<seq.length;i++){
         arr[i] = (""+seq[i]);
      
      }
      return arr;
   }
   
   /*
    * method
    */
   public static String[] toStringArray(ArrayList<String> seq){
   
      String[] arr = new String[seq.size()];
      for (int i=0;i<seq.size();i++){
         arr[i] = seq.get(i);
      }
      return arr;
   }
}
