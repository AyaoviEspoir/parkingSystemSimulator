/**
File : Route.java
Date : 29/03/2016
Author : Ayaovi Espoir Djissenou
*/

import java.util.List;
import java.util.ArrayList;
import java.util.Hashtable;

public class Route
{
    // private static List<ParkingArea> parkingAreas;
    // private static List<DisplayBoard> displayBoards;
    private static Dijkstra dijkstra;
    // private static int parkingAreaVertexIndex[];
    
    public Route()
    {
        // verteces = new ArrayList<Vertex>();
        // initialize();
        dijkstra = new Dijkstra();
        System.out.println("Route created...");
    }
    
    public static List<Vertex> getShortestPath(Vertex source, Vertex destination)
    {
        return dijkstra.getShortestPath(source, destination);
    }
    
    /**
    Returns any DisplayBoard at the given Vertex
    */
    public static DisplayBoard getDisplayBoard(Vertex vertex)
    {
        for (DisplayBoard displayBoard : Config.displayBoards)
        {
            if (displayBoard.getVertex().equals(vertex))
            {
                return displayBoard;
            }
        }
        return null;
    }
    
    public static void main(String args[])
    {
        // initialize();
        dijkstra = new Dijkstra();
    }
    
    public static void initialize() {}
}
