# a simple makefile.
default:
	javac -g ParkingSystem.java
gui:
	javac -g Driver.java
all:
	javac -g *.java
clean:
	rm -rf *.class
