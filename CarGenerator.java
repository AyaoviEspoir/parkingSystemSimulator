import java.util.stream.IntStream;
import java.util.Hashtable;

public class CarGenerator
{
	private static int limit;
	//private static String directory;
    private static String colors[];
	private static int carIDs[];
	private static int carArrivalTimes[];
	private static int carParkingDurations[]; 
	
	public CarGenerator(int limit, String colors[])
	{
		this.limit = limit;
		//this.directory = directory
        this.colors = colors;
	}
	
	public static void generate()
	{
		carIDs = IntStream.generate(()->{return (int)(123456 + Math.random() * 124457);}).distinct().limit(limit).toArray();
		//i.limit(limit).distinct();
		//carIDs = i.toArray();
		
		carArrivalTimes = IntStream.generate(()->{return (int)(10 + Math.random() * 1000);}).limit(limit).toArray();
		//i.limit(limit);
		//carArrivalTimes = i.toArray();
		
		carParkingDurations = IntStream.generate(()->{return (int)(30 + Math.random() * 20000);}).limit(limit).toArray();
		//i.limit(limit);
		//carParkingDurations = i.toArray();
        writeToFile();
	}
	
	private static void writeToFile()
	{
		String data[] = new String[limit];  // times 4 because of color, ID, araivalTime, parkingDuration.
        
        for (int i = 0; i < limit; i++)
        {
            data[i] = colors[(int)(Math.random() * colors.length)] + "\t" + carIDs[i] + "\t" + carArrivalTimes[i] + "\t" + carParkingDurations[i];
        }
        CCFileWriter writer = new CCFileWriter("../res/cars.txt", data);
        writer.writeToFile();
	}
    
    public static void main(String args[])
    {
        limit = 1000;
        colors = new String[]{"RED", "YELLOW", "BLUE", "VISITOR", "DISABLE"};
        generate();
        
        //System.out.println(carIDs.length);
		//System.out.println(carArrivalTimes.length);
		//System.out.println(carParkingDurations.length);
        
        //writeToFile();
    }
}
