public class Location
{
    public final double x;
    public final double y;
    
    public Location()
    {
        // default location.
        x = 0;
        y = 0;
    }
    
    public Location(Location location)
    {
        // default location.
        x = location.x;
        y = location.y;
    }
    
    public Location(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public double distanceTo(Location location)
    {
        // System.out.println("Computing distance between two loctions...");
        return Math.sqrt((x - location.x) * (x - location.x) + (y - location.y) * (y - location.y));
    }
    
    public boolean equals(Location location)
    {
        // System.out.println("Comparing two locations...");
        return (x == location.x && y == location.y);
    }
        
    public String toString()
    {
        return ("("+x+", "+y+")");
        
    }
}