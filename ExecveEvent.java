//package simulator; 
/**
 * An Execve event represents the creation of a program execution.
 * 
 * @author Stephan Jamieson
 * @version 8/3/15
 */
class ExecveEvent extends Event {

    private int carID;
    // private Kernel kernel;
    // private int priority;
    
    public ExecveEvent(long arrivalTime, int carID) 
    {
        super(arrivalTime);
        // this.priority=priority;
        this.carID = carID;
        // this.kernel=kernel;
    }
        
    
    /**
     * Obtain the name of the program that must be run.
     */
    public int getCarID() 
    {
        return carID;
    }
    
    /**
     * Obtain the priority of the program that must be run.
     */
   // int getPriority() { return priority; }

    /**
     * starts the CarThread matching the carID.
     */
    public void process() 
    {
      // TRACE.SYSCALL(SystemCall.EXECVE, getProgramName());
      // Config.getSimulationClock().logSystemCall();
      // kernel.syscall(SystemCall.EXECVE, getProgramName(), getPriority());
      // TRACE.SYSCALL_END();
      Config.movingCars.add(Config.cars.get(new Integer(carID)));
      System.out.println("Added a car to movingCars queues ........");
      Config.carThreads.get(new Integer(carID)).start();
      System.out.println("Started the car carThread ........");
    }
    
    public String toString() 
    { 
      return "ExecveEvent("+this.getTime()+", "+this.getCarID() /*+"["+this.getPriority()+"])"*/+")"; 
    }

}
