/**
 * A CarThread continuously advances a car at a specified speed
 * from source to destination. It terminates at the end of the lifecycle. 
 * 
 * @author Ayaovi Espoir Djissenou 
 * @version 02/04/2016
 */

public class CarThread extends Thread
{
	// private final int carID;
	private Car car;

	public CarThread(Car car)
	{
		this.car = car;
	}

	public void run()
	{
		while(!Config.stopSimulation)
		{
			if (!car.hasReachedDestination())
			{
				try
				{
					car.advance();
					Thread.sleep(5);
				}
				catch (InterruptedException e)
	            {
	                e.printStackTrace();
	            }
			}
			else 
			{
				try
				{
					/* we may want to remove this car from the currently running cars queue. */
					Thread.sleep(20);
				}
				catch (InterruptedException e)
	            {
	                e.printStackTrace();
	            }
			}
			
		}
		// if (car.hasReachedDestination())
		// {
		// 	// put the thread to sleep.
		// 	
	}
}