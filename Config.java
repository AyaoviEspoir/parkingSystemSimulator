/**
 * The class manages the configuration and running of a simulation.
 * <p>
 * It collates the hardware components: clock, cpu, and I/O devices.
 * Classes such as concrete Kernels can access these components here. 
 * 
 * @author Stephan Jamieson
 * @modified by Ayaovi Espoir Djissenou
 * @version 13/3/16
 */

// import java.util.HashMap;
// import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Hashtable;
//
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
//
import java.util.Scanner;

public class Config 
{
    public static AtomicInteger systemClock;
    public static volatile boolean stopSimulation;
    private static EventScheduler scheduler;
    public static Route roadMap;
    public static String diskColors[];
    public static int numberOfParkingAreas;
    public static List<Vertex> verteces;
    public static Vertex entrances[];
    // public static Vertex entranceTwo;
    public static List<ParkingArea> parkingAreas;
    public static List<DisplayBoard> displayBoards;
    public static int parkingAreaCapacity[];
    public static int parkingAreaColorAllocation[];
    public static int parkingAreaVertexIndex[];
    public static int displayBoardVertexIndex[];
    public static int parkingAreasCoords[];
    public static Hashtable<Integer, Car> cars;
    public static Hashtable<Integer, CarThread> carThreads;
    public static int carsID[];
    public static int carsArrivalTime[];
    public static int carsParkingDuration[];
    public static List<Car> movingCars;


    public Config() {}
    
    // static long getSystemClock() { return Config.systemClock; }
    static EventScheduler getEventScheduler() { return Config.scheduler; }

    /**
	 * Obtain the roadMap used in the current configuration.
	 */
    public static Route getRoadMat() { return Config.roadMap; }
	
	/**
	 * Create an initial simulation configuration that uses the given kernel, context switch cost and 
	 * system call cost.
	 */
    public static void init() 
    {
        // Config.clock=new SimulationClock(sysCallCost, cSwitchCost);
        // Config.clock = new SimulationClock();
        System.out.println("Inside Config.init()...");
        Config.stopSimulation = false;
        Config.scheduler = new EventScheduler();
        Config.systemClock = new AtomicInteger(0);
        Config.movingCars = new ArrayList<Car>();
        //Config.scheduler = new EventScheduler();
        Config.roadMap = new Route();

        CCFileReader reader = new CCFileReader("../res/diskColors.txt", "#");
        diskColors = reader.getStringArray();
        
        reader.setFilename("../res/parkingAreaAllocation.txt");
        numberOfParkingAreas = reader.getNumberOfLines();
        parkingAreaCapacity = reader.getRangeOfElements(0, 1);
        parkingAreaColorAllocation = reader.getRangeOfElements(1, diskColors.length + 1);     /* 2-D arrays in 1-D */
        
        reader.setFilename("../res/parkingAreaCoordinates.txt");
        parkingAreasCoords = reader.getIntegerArray();

        createVerteces();
        createParkingAreaList();
        createDisplayBoardList();

        CarGenerator carGenerator = new CarGenerator(10, Config.diskColors);
        carGenerator.generate();

        createCarThreads();

        createEvents();
    }


    private static void createEvents()
    {
        assert(carsArrivalTime.length == carsParkingDuration.length);   /* because they are both related to the number
        of cars in the system.*/
        for (int i = 0; i < carsArrivalTime.length; i++)
        {
            Config.scheduler.schedule(new ExecveEvent(carsArrivalTime[i], carsID[i]));
            Config.scheduler.schedule(new WakeUpEvent(carsArrivalTime[i] + carsParkingDuration[i], carsID[i]));
        }
    }

    private static void createCarThreads()
    {
        carThreads = new Hashtable<Integer, CarThread>();
        cars = new Hashtable<Integer, Car>();
        // List<Car> cars = new ArrayList<Car>();
        Car car = new Car(123456, "RED", 389, 6864, Config.displayBoards.get(1).getVertex());
        car.setSpeed(5);
        carsID = new int[]{car.getID()};
        carsArrivalTime = new int[]{car.getArrivalTime()};
        carsParkingDuration = new int[]{car.getParkingDuration()};
        cars.put(new Integer(car.getID()), car);
        carThreads.put(new Integer(car.getID()), new CarThread(car));
    }


    private static void createParkingAreaList()
    {
        System.out.println("Inside Config.createParkingAreaList()...");
        parkingAreas = new ArrayList<ParkingArea>();

        // CREATE LIST OF PARKING AREAS.
        for (int i = 0; i < numberOfParkingAreas; i++)
        {
            Hashtable<String, Integer> colorAllocation = new Hashtable<String, Integer>();
            for (int j = 0; j < diskColors.length; j++)
            {
                colorAllocation.put(diskColors[j], parkingAreaColorAllocation[ i * diskColors.length + j]);
            }
            
            ParkingArea parkingArea = new ParkingArea(i + 1, parkingAreaCapacity[i], verteces.get(parkingAreaVertexIndex[i]), colorAllocation);
            parkingAreas.add(parkingArea);
            //System.out.println(parkingArea.getName() + " " + parkingArea.getLocation());
        }
        System.out.println("Created " + parkingAreas.size() + " parkingAreas...");
    }

    /**
     * can only be called after createParkingAreaList().
     */
    private static void createDisplayBoardList()
    {
        System.out.println("Inside Config.createDisplayBoardList()...");
        displayBoards = new ArrayList<DisplayBoard>();
        entrances = new Vertex[2];
        
        // CREATE DISPLAY BOARDS.
        for (int i = 0; i < numberOfParkingAreas + 2; i++)
        {
            DisplayBoard displayBoard = new DisplayBoard(i - 1, verteces.get(displayBoardVertexIndex[i]));
            
            if (i == 0 || i == 1)
            {
                for (ParkingArea parkingArea : parkingAreas)
                {
                    displayBoard.addParkingArea(parkingArea);
                }
                entrances[i] = displayBoard.getVertex();
            }
            else
            {
                displayBoard.addParkingArea(parkingAreas.get(i - 2));
            }
            //System.out.println(displayBoard.getName() + " " + displayBoard.getLocation());
            displayBoards.add(displayBoard);
            
            //System.out.println("DISPLAY BOARD " + displayBoard.ID + "\n" + displayBoard);
        }
        System.out.println("Created " + displayBoards.size() + " displayBoards...");
    }

    /**
    Creates the verteces in the system.
    */
    private static void createVerteces()
    {
        verteces = new ArrayList<Vertex>();
        // String vertecesName[] = new String[]{"S0", "DB0", "E3", "E4", "DB1", "P1", "E7", "E8", 
        //                                     "E9", "E10", "E11", "DB4", "P4", "DB5", "P5", "E16",
        //                                     "E17", "DB6", "P6", "DB2", "P2", "DB3", "P3"};
        // mark all the vertices 
        Vertex E1 = new Vertex("DB0", new Location(815, 490));
        Vertex E2 = new Vertex("E2", new Location(745, 490));
        Vertex E3 = new Vertex("E3", new Location(85, 490));
        Vertex E4 = new Vertex("E4", new Location(745, 450));
        Vertex E5 = new Vertex("DB1", new Location(791, 450));
        Vertex E6 = new Vertex("P1", new Location(791, 305));
        Vertex E7 = new Vertex("E7", new Location(745, 360));
        Vertex E8 = new Vertex("E8", new Location(85, 360));
        Vertex E9 = new Vertex("E9", new Location(745, 260));
        Vertex E10 = new Vertex("E10", new Location(85, 260));
        Vertex E11 = new Vertex("E11", new Location(745, 75));
        Vertex E12 = new Vertex("DB4", new Location(600, 75));
        Vertex E13 = new Vertex("P4", new Location(600, 175));
        Vertex E14 = new Vertex("DB5", new Location(300, 75));
        Vertex E15 = new Vertex("P5", new Location(300, 175));
        Vertex E16 = new Vertex("E16", new Location(85, 75));
        Vertex E17 = new Vertex("E17", new Location(195, 75));
        Vertex E18 = new Vertex("DB6", new Location(195, 30));
        Vertex E19 = new Vertex("P6", new Location(250, 30));
        Vertex E20 = new Vertex("DB2", new Location(415, 360));
        Vertex E21 = new Vertex("P2", new Location(415, 445));
        Vertex E22 = new Vertex("DB3", new Location(85, 670));
        Vertex E23 = new Vertex("P3", new Location(140, 670));
        Vertex E24 = new Vertex("DB-1", new Location(10, 490));

        // set the edges and weight
        E1.adjacencies = new Edge[]{ new Edge(E2, 70) };
        E2.adjacencies = new Edge[]{ new Edge(E3, 660), new Edge(E4, 40) };
        E3.adjacencies = new Edge[]{ new Edge(E2, 660), new Edge(E8, 130), new Edge(E22, 180), new Edge(E24, 75)};
        E4.adjacencies = new Edge[]{ new Edge(E2, 40), new Edge(E5, 46), new Edge(E7, 90) };
        E5.adjacencies = new Edge[]{ new Edge(E4, 40), new Edge(E6, 145) };
        E6.adjacencies = new Edge[]{ new Edge(E5, 145) };
        E7.adjacencies = new Edge[]{ new Edge(E4, 90), new Edge(E9, 100), new Edge(E20, 330) };
        E8.adjacencies = new Edge[]{ new Edge(E3, 130), new Edge(E10, 100), new Edge(E20, 330) };
        E9.adjacencies = new Edge[]{ new Edge(E7, 100), new Edge(E10, 660), new Edge(E11, 185) };
        E10.adjacencies = new Edge[]{ new Edge(E8, 100), new Edge(E9, 660), new Edge(E16, 185) };
        E11.adjacencies = new Edge[]{ new Edge(E9, 185), new Edge(E12, 65) };
        E12.adjacencies = new Edge[]{ new Edge(E11, 65), new Edge(E13, 100), new Edge(E14, 460) };
        E13.adjacencies = new Edge[]{ new Edge(E12, 100) };
        E14.adjacencies = new Edge[]{ new Edge(E12, 460), new Edge(E15, 100), new Edge(E17, 25) };
        E15.adjacencies = new Edge[]{ new Edge(E14, 100) };
        E16.adjacencies = new Edge[]{ new Edge(E10, 185), new Edge(E17, 110) };
        E17.adjacencies = new Edge[]{ new Edge(E14, 25), new Edge(E16, 110), new Edge(E18, 45) };
        E18.adjacencies = new Edge[]{ new Edge(E17, 45), new Edge(E19, 130) };
        E19.adjacencies = new Edge[]{ new Edge(E18, 130) };
        E20.adjacencies = new Edge[]{ new Edge(E7, 330), new Edge(E8, 330), new Edge(E21, 85) };
        E21.adjacencies = new Edge[]{ new Edge(E20, 100) };
        E22.adjacencies = new Edge[]{ new Edge(E3, 180), new Edge(E23, 45) };
        E23.adjacencies = new Edge[]{ new Edge(E22, 45) };
        E24.adjacencies = new Edge[]{ new Edge(E3, 75) };
        
        // add verteces to list.
        verteces.add(E1);
        verteces.add(E2);
        verteces.add(E3);
        verteces.add(E4);
        verteces.add(E5);
        verteces.add(E6);
        verteces.add(E7);
        verteces.add(E8);
        verteces.add(E9);
        verteces.add(E10);
        verteces.add(E11);
        verteces.add(E12);
        verteces.add(E13);
        verteces.add(E14);
        verteces.add(E15);
        verteces.add(E16);
        verteces.add(E17);
        verteces.add(E18);
        verteces.add(E19);
        verteces.add(E20);
        verteces.add(E21);
        verteces.add(E22);
        verteces.add(E23);
        verteces.add(E24);

        System.out.println(verteces.get(0).getLocation());
        
        //parkingAreaVertexIndex = new int[]{5, 20, 22, 12, 14, 18};
        parkingAreaVertexIndex = new int[numberOfParkingAreas];
        String parkingAreaNames[] = new String[]{"P1", "P2", "P3", "P4", "P5", "P6"};

        for (int i = 0; i < numberOfParkingAreas; i++)
        {
            int j = 0;
            for (Vertex vertex : verteces)
            {
                if (vertex.name.equals(parkingAreaNames[i]))
                {
                    //System.out.println(j);
                    parkingAreaVertexIndex[i] = j;
                    break;
                }
                j++;
            }
        }

        String displayBoardNames[] = new String[]{"DB-1", "DB0", "DB1", "DB2", "DB3", "DB4", "DB5", "DB6"};
        displayBoardVertexIndex = new int[numberOfParkingAreas + 2];

        for (int i = 0; i < numberOfParkingAreas + 2; i++)
        {
            int j = 0;
            for (Vertex vertex : verteces)
            {
                if (vertex.name.equals(displayBoardNames[i]))
                {
                    displayBoardVertexIndex[i] = j;
                    break;
                }
                j++;
            }
        }
    }

	/** 
	 * Run the configured simulation (the init() and buildConfiguration() methods must be called first).
	 */
    public static void run() {
        // Config.clock.setSystemTime(0);
        new SystemTimer();
        Config.scheduler.run();
	}
	   
    /**
	 * Complete the simulation configuration by uploading the given config file.
	 */
    public static void buildConfiguration(String directoryname) 
    {
        // try {
            // final BufferedReader reader = new BufferedReader(new FileReader(filename));
            
            // String line = reader.readLine().trim();
            // while (line!=null) {
                // if (line.startsWith("#") || line.equals("")) {
                    // // It's a commment or blank line, ignore.
                // }
                // else if (line.startsWith("PROGRAM")) {
                    // Scanner scanner = new Scanner(line);
                    // scanner.next(); // Consume "PROGRAM"
                    // if (!scanner.hasNextInt()) {
                        // // Whoops, missing program start time.
                        // System.out.println("PROGRAM entry missing start time: \""+line+"\".");
                        // System.exit(-1);
                    // }
                    // final int startTime = scanner.nextInt();
                    // if (!scanner.hasNextInt()) {
                        // // Whoops, priority.
                        // System.out.println("PROGRAM entry missing priority: \""+line+"\".");
                        // System.exit(-1);
                    // }
                    // final int priority = scanner.nextInt();
                    // if (!scanner.hasNext()) {
                        // // Whoops, missing program name.
                        // System.out.println("PROGRAM entry missing program name: \""+line+"\".");
                        // System.exit(-1);
                    // }
                    // Config.scheduler.schedule(new ExecveEvent(startTime, scanner.next(), priority, Config.getKernel())); 
                // }
                // else if (line.startsWith("DEVICE")) {
                    // Scanner scanner = new Scanner(line);
                    // scanner.next(); // Consume "DEVICE"
                    // if (!scanner.hasNextInt()) {
                        // // Whoops, missing device ID
                        // System.out.println("DEVICE entry missing device ID: \""+line+"\".");
                        // System.exit(-1);
                    // }
                    // final int deviceID = scanner.nextInt();
                    // if (!scanner.hasNext()) {
                        // // Whoops, missing device ID
                        // System.out.println("DEVICE entry missing device type: \""+line+"\".");
                        // System.exit(-1);
                    // }
                    // final String deviceType = scanner.next();
                    // TRACE.SYSCALL(SystemCall.MAKE_DEVICE, deviceID, deviceType);
                    // Config.getSimulationClock().logSystemCall();
                    // Config.kernel.syscall(SystemCall.MAKE_DEVICE, deviceID, deviceType);
                    // TRACE.SYSCALL_END();
                // }
                // else {
                    // System.out.println("Unrecognised token in configuration file : \""+filename+"\".");
                    // reader.close();
                    // System.exit(-1);
                // }
                // line = reader.readLine();
            // }
            // reader.close();
        // }
        // catch (FileNotFoundException fileNFExc) {
            // System.out.println("File \""+filename+"\" not found.");
            // System.exit(-1);
        // }
        // catch (IOException ioExc) {
            // System.out.println("IO Error reading from \""+filename+"\".");
            // System.exit(-1);
        // }        
    }
}
