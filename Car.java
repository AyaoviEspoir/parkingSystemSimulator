/**
File : Car.java
Date : 29/03/2016
Author : Ayaovi Espoir Djissenou
*/
import java.util.List;
import java.util.ArrayList;


public class Car
{
    private final String color;                  // red, yeallow, blue.
    private final int arrivalTime;
    private final int parkingDuration;		    // how long the car will be parked for.
    private final int ID;                       // unique to each car.
    private static Location currentLocation;    // location of the car.
    private static Vertex source;               // finishing point of the car.
    private static Vertex destination;          // finishing point of the car.
    private static double speed;
    private static double vMotionFactor;        // determines how much to move vertically with respect to speed.
    private static double hMotionFactor;      // determines how much to move horizontally with respect to speed.
    private static List<Vertex> route;          // indicates the path that a car must follow from source to destination.
    private static int nextVertexIndex;
    
    public Car()
    {
        ID = 0;
        speed = 0;
        nextVertexIndex = 0;
        this.arrivalTime = 0;
        parkingDuration = 0;
        color = "RED";
        currentLocation = new Location();
        destination = null;
    }
    
    public Car(int id, String color, int arrivalTime, int parkingDuration, Vertex source)
    {
        ID = id;
        this.color = color;
        nextVertexIndex = 0;
        this.arrivalTime = arrivalTime;
        this.parkingDuration = parkingDuration;
        this.source = source;
        currentLocation = source.getLocation();
        checkForDisplayBoard(this.source);
        //destination = null;
    }
    
    public int getArrivalTime()
    {
        return arrivalTime;
    }

    public int getParkingDuration()
    {
        return parkingDuration;
    }

    public Location getLocation()
    {
        return currentLocation;
    }
    
    public String getColor()
    {
        return color;
    }
    
    public int getID()
    {
        return ID;
    }
    
    public double getSpeed()
    {
        return speed;
    }
    
    public void setSpeed(double newSpeed)
    {
        speed = newSpeed;
    }
    
    public void setDestination(Vertex newDestination)
    {
        System.out.println("setting car's destination to " + newDestination.name);
        destination = newDestination;
        route = Config.roadMap.getShortestPath(this.source, this.destination);
        nextVertexIndex++;
    }
    
    private void checkForDisplayBoard(Vertex vertex)
    {
        System.out.println("Inside Car.checkForDisplayBoard()...");
        DisplayBoard displayBoard = Config.roadMap.getDisplayBoard(vertex);
        
        System.out.println(displayBoard);

        if (displayBoard != null)
        {
            if (destination == null)
            {
                Vertex parkingAreaLocation = displayBoard.checkForAvailableParkingBay(color);
                
                if (parkingAreaLocation == null)
                {
                    System.out.println("NO AVAILABLE PARKING BAY...");
                    // maybe we could add the car to some sort of waiting list.
                }
                else
                {
                    setDestination(parkingAreaLocation);
                }
            }
            else
            {
                boolean parkingBayIsAvailable = displayBoard.checkForAvailableParkingBay(color, destination.getName());
                
                if (!parkingBayIsAvailable)
                {
                    Vertex parkingAreaLocation = displayBoard.checkForAvailableParkingBay(color);
                
                    if (parkingAreaLocation == null)
                    {
                        System.out.println("NO AVAILABLE PARKING BAY...");
                        // maybe we could add the car to some sort of waiting list.
                    }
                    else
                    {
                        source = vertex;
                        setDestination(parkingAreaLocation);
                    }
                }
            }
        }
    }

    public static boolean hasReachedDestination()
    {
        // TODO.
        return currentLocation.equals(destination.getLocation());
    }
    
    private static void advanceHorizontally(double distance)
    {
        if (currentLocation.x > route.get(nextVertexIndex).getLocation().x)
        {
            // meaning car is moving from right to left.
            if ((currentLocation.x - distance) <= route.get(nextVertexIndex).getLocation().x)
                distance = route.get(nextVertexIndex).getLocation().x - currentLocation.x;
            else
                distance *= -1.0;    
        }
        else if (currentLocation.x < route.get(nextVertexIndex).getLocation().x)
        {
            // meaning car is moving from left to right.
            if ((currentLocation.x + distance) >= route.get(nextVertexIndex).getLocation().x)
                distance = route.get(nextVertexIndex).getLocation().x - currentLocation.x;
        }
        
        // distance = route.get(nextVertexIndex).getLocation().x - currentLocation.x;
        currentLocation = new Location(currentLocation.x + distance, currentLocation.y);
    }
    
    private static void advanceVertically(double distance)
    {
        // if ((currentLocation.y + distance) >= route.get(nextVertexIndex).getLocation().y)
        //     distance = route.get(nextVertexIndex).getLocation().y - currentLocation.y;
        // currentLocation = new Location(currentLocation.x, currentLocation.y + distance);;
        if (currentLocation.y > route.get(nextVertexIndex).getLocation().y)
        {
            // meaning car is moving from top down.
            if ((currentLocation.y - distance) <= route.get(nextVertexIndex).getLocation().y)
                distance = route.get(nextVertexIndex).getLocation().y - currentLocation.y;
            else
                distance *= -1.0;
        }
        else if (currentLocation.y < route.get(nextVertexIndex).getLocation().y)
        {
            // meaning car is moving from butom up.
            if ((currentLocation.y + distance) >= route.get(nextVertexIndex).getLocation().y)
                distance = route.get(nextVertexIndex).getLocation().y - currentLocation.y;
        }
        
        // distance = route.get(nextVertexIndex).getLocation().x - currentLocation.x;
        currentLocation = new Location(currentLocation.x, currentLocation.y + distance);
    }

    private void advanceInStraightLine()
    {
        vMotionFactor = (currentLocation.x == route.get(nextVertexIndex).getLocation().x) ? 1.0 : 0.0;
        hMotionFactor = (currentLocation.y == route.get(nextVertexIndex).getLocation().y) ? 1.0 : 0.0;
        
        advanceVertically(vMotionFactor * speed);
        advanceHorizontally(hMotionFactor * speed);
        
        if (currentLocation.distanceTo(route.get(nextVertexIndex).getLocation()) == 0.0)
        {
            checkForDisplayBoard(route.get(nextVertexIndex));
            nextVertexIndex++;
        }
        System.out.println(this.toString() + " is now at " + this.getLocation());
        // if()
        // {
        //     // move vertically.
        //     advanceVertically(speed);
            
        //     if (Math.abs(currentLocation.x - route.get(nextVertexIndex).getLocation().x) < speed)
        //     {
        //         checkForDisplayBoard(route.get(nextVertexIndex));
        //     }

        //     if (currentLocation.x < route.get(nextVertexIndex).getLocation().x)
        //         nextVertexIndex++;
        // }
        // else if (currentLocation.y == route.get(nextVertexIndex).getLocation().y)
        // {
        //     // move horizontally.
        //     advanceHorizontally(speed);

        //     if (Math.abs(currentLocation.y - route.get(nextVertexIndex).getLocation().y) < speed)
        //     {
        //         checkForDisplayBoard(route.get(nextVertexIndex));
        //     }

        //     if (currentLocation.y < route.get(nextVertexIndex).getLocation().y)
        //         nextVertexIndex++;
        // }
    }

    public boolean advance()
    {
        // TODO
        assert(destination != null);
        // verticalMotionFactor
        // horizontalMotionFactor
        // if (currentLocation.distanceTo(route.get(nextVertexIndex).getLocation()) < speed)
        // {
        //     if (route.get(nextVertexIndex).isCornerVertex(route.get(nextVertexIndex - 1), route.get(nextVertexIndex + 1)))
        //     {
        //         // move horizontally and vertically.
        //         if (currentLocation.y == route.get(nextVertexIndex).getLocation().y)
        //         {
        //             advanceHorizontally(route.get(nextVertexIndex).getLocation().x - currentLocation.x);
        //             advanceVertically(currentLocation.x + speed - route.get(nextVertexIndex).getLocation().x);
        //         }
        //         else if (currentLocation.x == route.get(nextVertexIndex).getLocation().x)
        //         {
        //             advanceVertically(route.get(nextVertexIndex).getLocation().y - currentLocation.y);
        //             advanceHorizontally(currentLocation.y + speed - route.get(nextVertexIndex).getLocation().y);
        //         }

        //         // surely after this we have passed a vertex.
        //         nextVertexIndex++;
        //     }
        //     else
        //     {
        //         advanceInStraightLine();
        //     }
        // }
        // else
        // {
            
        // }
        if (!currentLocation.equals(destination.getLocation()))
        {
            advanceInStraightLine();
            return true;
        }
        
        return false;
    }

    public String toString()
    {
        return ID + " " + color;
    }
}
