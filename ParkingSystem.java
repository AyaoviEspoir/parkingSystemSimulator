/**
File : ParkingSystem.java
Date : 31/03/2016
Author : Ayaovi Espoir Djissenou
*/

public class ParkingSystem
{
	public ParkingSystem()
    {
        
    }
    
    public static void main(String args[])
    {
    	// Config config = new Config();
    	// config.init();
    	Config.init();
        
        new SystemTimer();

        try
        {
            Thread.sleep(10000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        Config.stopSimulation = true;

        // CREATE CARS.
        // CCFileReader reader = new CCFileReader("../res/cars.txt");
        // int numberOfCars = reader.getNumberOfLines();
        // String data[] = reader.getData();

        Car car = new Car(123456, "RED", 389, 6864, Config.displayBoards.get(0).getVertex());
        // car.setSpeed(5);
        // System.out.println(car.getLocation());
        
        // int count = 0;
        // while(/*count < 100 && */!car.hasReachedDestination())
        // {
        // 	car.advance();
        // 	System.out.println(car.getLocation());
        // 	// count++;
        // }
    }
}
