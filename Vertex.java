/**
File : Vertex.java
Date : 29/03/2016
Author : Ayaovi Espoir Djissenou
*/

class Vertex implements Comparable<Vertex>
{
    public final String name;
    private final Location location;
    public Edge[] adjacencies;
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;
    public Vertex(String argName, Location location) { name = argName; this.location = location; }
    
    public String toString() { return name; }
    
    public int compareTo(Vertex other)
    {
        return Double.compare(minDistance, other.minDistance);
    }
    
    public Location getLocation()
    {
        // return new Location(location);
        return location;
    }

    public boolean isCornerVertex(Vertex previous, Vertex next)
    {
        // simply compute dot product.
        return ((location.x - previous.getLocation().x) * (location.x - next.getLocation().x) 
            + (location.y - previous.getLocation().y) * (location.y - next.getLocation().y)) == 0.0;
    }
    
    public String getName()
    {
        //System.out.println("vertex name is " + name + "...");
        return name;
    }
    
    public boolean equals(Vertex otherVertex)
    {
        return location.equals(otherVertex.getLocation());
        // we can simply call on equals from Location but for it to work, 
        // it needs to be defined as static in Location which is not convenient 
        // in this case as it requires Location.x and Location.y to be static as 
        // well. But doing does not alllows us creating multiple objects of Location.
        // return (otherVertex.getLocation().x == location.x && otherVertex.getLocation().y == location.y);
    }
}