/**
File : ParkingArea.java
Date : 29/03/2016
Author : Ayaovi Espoir Djissenou
*/

import java.util.*;

public class ParkingArea
{
    private final int CAPACITY;                                     // total capacity of the parking area.
    public final int ID;                                            // unique to each parking area.
    //private static final Location location;                       // location of the parking area.
    private final Vertex vertex;
    private final Hashtable<String, Integer> bayColorsAllocation;   // disk color that have legal access (dynamically configured).
    private Hashtable<String, Integer> occupied;
    private List<Car> legalVehicles;                                // is this necessary?
    private List<Car> illegalVehicles;
    public static boolean isFull;                                   // needs to be updated regularly.
    
    // default constructor.
    public ParkingArea()
    {
        CAPACITY = 0;
        ID = 0;
        isFull = true;
        //location = new Location();
        vertex = null;
        bayColorsAllocation = new Hashtable<String, Integer>();
        occupied = new Hashtable<String, Integer>();
        illegalVehicles = new ArrayList<Car>();
        legalVehicles = new ArrayList<Car>();
    }
    
    public ParkingArea(int id, int capacity, Vertex vertex, Hashtable<String, Integer> bayColorsAllocation)
    {
        ID = id;
        CAPACITY = capacity;
        isFull = false;
        //this.location = location;
        this.vertex = vertex;
        this.bayColorsAllocation = bayColorsAllocation;
        occupied = new Hashtable<String, Integer>();
        illegalVehicles = new ArrayList<Car>();
        legalVehicles = new ArrayList<Car>();
    }
    
    public void addCar(Car car)
    {
        if (occupied.get(car.getColor()) < bayColorsAllocation.get(car.getColor()))
        {
            occupied.put(car.getColor(), occupied.get(car.getColor()) + 1);
            legalVehicles.add(car);
            checkIfFull();		// necessary.
        }
        else 
        {
            /* if no parking is available but a driver still drove in, then 
            he/she is trespassing. */
            illegalVehicles.add(car);
        }
    }
    
    public void removeCar(Car car)
    {
        // TODO
        occupied.put(car.getColor(), occupied.get(car.getColor()) - 1);
        checkIfFull();
    }
    
    private void checkIfFull()
    {
		isFull = true;
		Enumeration colors;
        colors = bayColorsAllocation.keys();
		
		//for (String color : bayColorsAllocation.keys())
		while (colors.hasMoreElements())
		{
			String color = (String) colors.nextElement();
			if (occupied.get(color) == null || occupied.get(color) < bayColorsAllocation.get(color))
			{
				isFull = false;
				break;
			}
		}
	}
    
    public Location getLocation()
    {
        return vertex.getLocation();
    }
    
    public int getCapacity()
    {
        return CAPACITY;
    }
    
    public int getID()
    {
        return ID;
    }

    public String getName()
    {
        return vertex.name;
    }

    public Vertex getVertex()
    {
        return vertex;
    }
    
    public Hashtable<String, Integer> getAvailableBays()
    {
        Hashtable<String, Integer> availableBays = new Hashtable<String, Integer>();
        Enumeration colors;
        colors = bayColorsAllocation.keys();
        
        while (colors.hasMoreElements())
        {
            String color = (String) colors.nextElement();
            if (occupied.get(color) != null)
				availableBays.put(color, bayColorsAllocation.get(color)-occupied.get(color));
			else
				availableBays.put(color, bayColorsAllocation.get(color));
        }
        
        return availableBays;
    }
}
