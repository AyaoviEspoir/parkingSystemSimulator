//import java.util.List;
//import java.util.Hashtable;
//import java.util.ArrayList;
import java.util.*;

public class DisplayBoard
{
    private List<ParkingArea> parkingAreas;
    private final Vertex vertex;
    public final int ID;              // unique to each display board.
    
    public DisplayBoard()
    {
		ID = 0;
        parkingAreas = new ArrayList<ParkingArea>();
        vertex = null;
    }
    
    public DisplayBoard(int id, Vertex vertex)
    {
		ID = id;
        parkingAreas = new ArrayList<ParkingArea>();
        this.vertex = vertex;
    }
    
    public Location getLocation()
    {
        return vertex.getLocation();
    }
    
    public Vertex getVertex()
    {
        return vertex;
    }

    public String getName()
    {
        return vertex.name;
    }
    
    public Vertex checkForAvailableParkingBay(String color)
    {
        // TODO.
        for (ParkingArea parkingArea : parkingAreas)
        {
            Hashtable<String, Integer> availableBays = parkingArea.getAvailableBays();
            if (availableBays.get(color) != null && availableBays.get(color) > 0)
                return parkingArea.getVertex();
        }
        return null;
    }
    
    public boolean checkForAvailableParkingBay(String color, String parkingAreaName)
    {
        // TODO.
        for (ParkingArea parkingArea : parkingAreas)
        {
            if (parkingArea.getName().equals(parkingAreaName))
            {
                Hashtable<String, Integer> availableBays = parkingArea.getAvailableBays();
                if (availableBays.get(color) != null && availableBays.get(color) > 0)
                    return true;
                break;
            }
        }
        return false;
    }
    
    public void addParkingArea(ParkingArea parkingArea)
    {
        parkingAreas.add(parkingArea);
    }
    
    public String toString()
    {
        String display = "";
        
        for (ParkingArea parkingArea : parkingAreas)
        {
            if (!parkingArea.isFull)
            {
				display += "Parking Area " + parkingArea.ID + "\n";
                Hashtable<String, Integer> availableBays = parkingArea.getAvailableBays();
                Enumeration colors;
				colors = availableBays.keys();
                //for (String color : availableBays.keys())
                while (colors.hasMoreElements())
                {
					String color = (String) colors.nextElement();
					if (availableBays.get(color) != 0)
					{
						display += availableBays.get(color) + " " + color + ", ";
					}
				}
				display = display.substring(0, display.length()-2);		/* removes the triling ", " in display */
				display += "\n";
            }
        }
        
        return display;
    }
}
