/**
 * Started on the 08/01/2015
 */

//package test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

class CCFileWriter 
{

	private static String fileName;
	private static String data[];
   
	public CCFileWriter(String fileName, String data[]) {
		this.fileName = fileName;
		this.data = data;
	}
   
   /*
    * method.
    */
	public static void writeToFile()
   {
		BufferedWriter bfwriter = null;
		try{
			File file = new File(fileName);
			if (!file.exists())
				file.createNewFile();
         FileWriter writer = new FileWriter(file);
         bfwriter = new BufferedWriter(writer);
         
         for (int i=0;i<data.length;i++){
            bfwriter.write(data[i]);
            bfwriter.newLine();
         }
      }
      catch(IOException e){
         e.printStackTrace();
      }
      finally{
         try{
            if (bfwriter!=null)
            bfwriter.close();
         }
         catch(Exception e){
            e.printStackTrace();
         }
      }
   }
}
