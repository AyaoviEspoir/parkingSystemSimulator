/**
 * A SystemTimer (i) provides support for simulated time, and (ii) support for
 * the setting of timeout interrupts by kernel code. 
 * 
 * @author Ayaovi Espoir Djissenou 
 * @version 02/04/2016
 */

// import java.util.concurrent.CountDownLatch;

public class SystemTimer extends Thread 
{
    public SystemTimer() { start(); }

    public void run()
    {
        while (!Config.stopSimulation)
        {
            try 
            {
                Config.systemClock.getAndAdd(10);
                // System.out.println("System time is now: " + getSystemTime());
                Thread.sleep(10);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

    /** 
     * Obtain the current system time.
     */
    public int getSystemTime()
    {
        return Config.systemClock.get();
    }
    
    /**
     * Obtain the amount of time the CPU has been idle.
     */
    // public long getIdleTime();

    /**
     * Obtain the amount of time the system has spent executing in user space.
     */
    // long getUserTime();

    /**
     * Obtain the amount of time the system has spent executing in kernel space.
     */
    // long getKernelTime();

    
    /**
     * Schedule a timer interrupt for <code>timeUnits</code> time units in the future.
	 * The given handler receives a call back when it occurs.
     */
    // void scheduleInterrupt(int timeUnits, InterruptHandler handler, Object... varargs);
    
    /**
     * Cancel timer interrupt for given process ID.
     */
    // void cancelInterrupt(int processID);
}
