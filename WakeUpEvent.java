 // package simulator; 
/**
 * A WakeUp event occurs when an I/O operation completes.
 * 
 * @author Stephan Jamieson
 * @version 8/3/15
 */
class WakeUpEvent extends Event 
{
    
    private int carID;
    private int index;
    // private int processID;
    // private EventHandler<WakeUpEvent> handler;
    
    /**
     * Create a WakeUpEvent for the given process waiting on an I/O operation on the given device.
     */
    public WakeUpEvent(long systemTime, int carID/*, int processID, EventHandler<WakeUpEvent> handler*/) {
        super(systemTime);
        this.carID = carID;
        index = 0;
        // this.processID=processID;
        // this.handler=handler;
    }
    
    /**
     * Obtain the I/O device.
     */
    public int getCarID() { return carID; }
    
    /**
     * Obtain the waiting process.
     */
    // public int getProcessID() { return processID; }

    /**
     * Process this event.
     */
    public void process() 
    {
        // TODO
        /* change the destination. */
        Config.cars.get(new Integer(carID)).setDestination(Config.entrances[index]);
        index %= 2;
    }
    
    public String toString() 
    { 
        return "WakeUpEvent("+this.getTime()+", "+this.getCarID()+")"; 
    }        
}
